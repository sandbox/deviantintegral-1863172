<?php

/**
 * @file
 * Utility functions for Relation ACL.
 */

/**
 * Return the Relation types that are listed as next relations in a given
 * relation ACL configuration.
 *
 * @param $relation_acl
 *   The relation ACL configuration to search.
 *
 * @return
 *   An array of relation type names.
 */
function relation_acl_relation_types($relation_acl) {
  $relation_types = array();

  foreach ($relation_acl->rules as $entity => $bundles) {
    foreach ($bundles as $bundle) {
      if (!empty($bundle['next_relations'])) {
        $relation_types = array_merge($relation_types, array_keys($bundle['next_relations']));
      }
    }
  }

  return array_values(array_unique($relation_types));
}

/**
 * Return an array of all grants that a Relation ACL rule grants to a an
 * entity's author.
 *
 * @param $rule
 *   The Relation ACL rule containing possible grants.
 * @param $entity.
 *   An entity contianing a uid property to check against.
 *
 * @return
 *   An array of grants keyed by entity UID.
 */
function relation_acl_rule_grants($rule, $entity) {
  $grants = array();

  if (isset($rule['grant_view']) || isset($rule['grant_update']) || isset($rule['grant_delete'])) {
    $permissions = array('grant_view', 'grant_update', 'grant_delete');
    if (isset($entity->uid)) {
      $grants[$entity->uid] = array(
        'grant_view' => 0,
        'grant_update' => 0,
        'grant_delete' => 0,
      );

      foreach ($permissions as $permission) {
        if (isset($rule[$permission]) && $rule[$permission] == TRUE) {
          $grants[$entity->uid][$permission] = TRUE;
        }
      }
    }
  }

  return $grants;
}

/**
 * Traverse a relation graph and return all grants as defined by this module.
 *
 * @param $entity
 *   The initial entity to start with. This is almost always a node being
 *   passed in from the node access subsystem.
 * @param $entity_type
 *   The type of entity, such as 'node'.
 * @param $bundle
 *   The bundle of the entity, such as 'article'.
 * @param $traversing
 *   Optional parmameter to indicate that we are recursing through this
 *   function, and to not reset our static guard of seen entities.
 *
 * @return
 *   An array of grants, keyed first by Relation ACL type and then user ID.
 */
function relation_acl_traverse($entity, $entity_type, $bundle, $traversing = FALSE) {
  $grants = array();

  // Get the name of the property of the entity's ID.
  $info = entity_get_info($entity_type);
  $id = $info['entity keys']['id'];

  // Store a list of all entities we have already traversed to prevent infinite
  // recursion.
  if (!$traversing) {
    // We do this here since if we are recursing, we know we've loaded this
    // file.
    drupal_static_reset(__FUNCTION__);
  }
  $traversed_entities = &drupal_static(__FUNCTION__);
  if (isset($traversed_entities[$entity_type]) && in_array($entity->{$id}, $traversed_entities[$entity_type])) {
    return $grants;
  }

  $traversed_entities[$entity_type][] = $entity->{$id};

  // For each type of Relation ACL rule, generate grants.
  foreach (relation_acl_find($entity_type, $bundle) as $relation_acl) {
    $rule = $relation_acl->rules[$entity_type][$bundle];

    // Does this rule have any grants set?
    $rule_grants = relation_acl_rule_grants($rule, $entity);
    if (!empty($rule_grants)) {
      $grants[$relation_acl->rid] = $rule_grants;
    }

    // See if there's a next relationship in this rule.
    foreach ($rule['next_relations'] as $next_relation => $enforce_direction) {
      $result = relation_query($entity_type, $entity->{$id})
        ->propertyCondition('relation_type', $next_relation)
        ->execute();

      // Load the relation, and traverse all of it's endpoints.
      $relations = relation_load_multiple(array_keys($result));
      foreach ($relations as $relation) {
        // If we are enforcing direction on this relation, check to see if we
        // are looking at a relation target.
        if ($enforce_direction) {
          $endpoint = $relation->endpoints[LANGUAGE_NONE][1];
          if ($endpoint['entity_type'] == $entity_type && $endpoint['entity_id'] == $entity->{$id}) {
            continue;
          }
        }
        _relation_acl_traverse_endpoints($relation->endpoints[LANGUAGE_NONE], $entity, $entity_type, $grants);
      }
    }
  }

  return $grants;
}

/**
 * Internal function to traverse the endpoints of a given relation.
 */
function _relation_acl_traverse_endpoints($endpoints, $entity, $entity_type, &$grants) {
  // Get the name of the property of the entity's ID.
  $info = entity_get_info($entity_type);
  $id = $info['entity keys']['id'];

  foreach ($endpoints as $endpoint) {
    // Exclude the current endpoint.
    if ($endpoint['entity_type'] != $entity_type || $endpoint['entity_id'] != $entity->{$id}) {
      // Load the endpoint's entity and find it's bundle type.
      $next_entity = reset(entity_load($endpoint['entity_type'], array($endpoint['entity_id'])));

      // Seriously, this should be an API function in core or entity api?
      $next_info = entity_get_info($endpoint['entity_type']);
      $next_bundle = !empty($next_info['entity keys']['bundle']) ? $next_entity->{$next_info['entity keys']['bundle']} : $endpoint['entity_type'];

      $new_grants = relation_acl_traverse($next_entity, $endpoint['entity_type'], $next_bundle, TRUE);
      foreach ($new_grants as $rid => $ng) {
        if (!isset($grants[$rid])) {
          $grants[$rid] = array();
        }
        foreach ($ng as $grant => $permission) {
          if ($permission) {
            $grants[$rid][$grant] = $permission;
          }
        }
      }
    }
  }
}
/**
 * Given a relation, return the entities that are related below it in a tree
 * structure.
 *
 * @param $relation
 *   The relation to start with, traversing through it's targets.
 *
 * @return
 *   A list of entity IDs.
 */
function relation_acl_tree($relation, $r_index = 0) {
  $subentities = array();

  $endpoints = field_get_items('relation', $relation, 'endpoints');
  $subentities[$endpoints[$r_index]['entity_type']][] = $endpoints[$r_index]['entity_id'];

  $query = relation_query($endpoints[$r_index]['entity_type'], $endpoints[$r_index]['entity_id'], !$r_index);
  $query->propertyCondition('relation_type', $relation->relation_type);
  $result = $query->execute();

  // Load the full relation.
  $relations = relation_load_multiple(array_keys($result));

  foreach ($relations as $subrelation) {
    $r = relation_acl_tree($subrelation, $r_index);
    foreach ($r as $entity_type => $entity_ids) {
      $subentities[$entity_type] = array_unique(array_merge($subentities[$entity_type], $entity_ids));
    }
  }

  // Returning a list of person node ids who work directly for this person.
  return $subentities;
}

/**
 * Given an entity, return all entities in the tree of relationships below it.
 *
 * @param $entity
 *   The entity to start with that is the root of the tree. This is not the
 *   relation entity, but an entity in a relation endpoint.
 * @param $entity_type
 *   The type of $entity such as 'node'.
 * @param $relation_type
 *   The relation type configuration to traverse.
 * @param $r_index
 *   The direction to build the tree in.
 *
 * @return
 *   An array of entity IDs, keyed by entity type.
 */
function relation_acl_tree_entity($entity, $entity_type, $relation_type, $r_index = 0) {
  $tree = array();

  $query = relation_query($entity_type, $entity->nid);
  $query->propertyCondition('relation_type', $relation_type->relation_type);
  $result = $query->execute();
  $relations = relation_load_multiple(array_keys($result));

  foreach ($relations as $relation) {
    $endpoints = field_get_items('relation', $relation, 'endpoints');
    $tree[$endpoints[$r_index]['entity_type']][] = $endpoints[$r_index]['entity_id'];

    $subtree = relation_acl_tree($relation, $r_index);
    foreach ($subtree as $entity_type => $entity_ids) {
      $tree[$entity_type] = array_unique(array_merge($tree[$entity_type], $entity_ids));
    }
  }
  return $tree;
}

/**
 * Rebuild access controls for all grants provided by this module that could be
 * changed by the modification of the given relation.
 *
 * @param $relation
 *   The relation entity causing the rebuild to occur.
 */
function relation_acl_queue_rebuild($relation) {
  $entity_info = entity_get_info('relation');
  $entity_bundle = !empty($entity_info['entity keys']['bundle']) ? $relation->{$entity_info['entity keys']['bundle']} : $type;

  $relation_acls = relation_acl_load_multiple();
  $nids = array();
  foreach ($relation_acls as $relation_acl) {
    $relation_types = relation_acl_relation_types($relation_acl);
    if (in_array($entity_bundle, $relation_types)) {
      // We need to find all nodes owned by all users who are in the subtree
      // of the described relation.
      $endpoints = field_get_items('relation', $relation, 'endpoints');
      $target = $endpoints[1];
      $target_entity = reset(entity_load($target['entity_type'], array($target['entity_id'])));

      // This gets us a list of all subentities.
      $entities = relation_acl_tree_entity($target_entity, $target['entity_type'], relation_type_load($entity_bundle));

      foreach ($entities as $type => $subnids) {
        foreach ($subnids as $nid) {
          // @todo This is needed to actually map person NIDs to user
          // profiles. Should be dynamic based on the relation_acl config.
          $users[] = relation_get_related_entity($type, $nid, 'user_profile');
        }
      }

      // Find all nodes owned by affected users.
      // @todo This might be optimized to be a single query instead of one
      // per account.
      foreach ($users as $account) {
        $query = db_select('node_access', 'na');
        $query->join('node', 'n', 'na.nid = n.nid');
        $query->condition('n.uid', $account->uid)
          ->condition('na.realm', $relation_acl->rid)
          ->fields('na', array('nid'))
          ->distinct();
        $result = $query->execute()->fetchAllAssoc('nid');
        $nids = array_merge($nids, array_keys($result));
      }
    }
  }

  // We now have a list of node IDs that need their relations to be rebuidl.
  $queue = DrupalQueue::get('relation_acl_nodes');
  $nids = array_values(array_unique($nids));
  foreach ($nids as $nid) {
    $item = new stdClass();
    // Tell queue workers to not process this item until at least 60 seconds
    // have passed. This reduces the workload if Relations are changed
    // quickly and prevents rebuilds if the ACL is reverted back due to a
    // mistake.
    $item->startTime = REQUEST_TIME + 60;
    $item->nid = $nid;
    $queue->createItem($item);
  }
}

